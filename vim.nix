with import <nixpkgs> {};

vim_configurable.customize {
  name = "vim";
  vimrcConfig.customRC = ''
    filetype on
    filetype indent on
    filetype plugin on

    syntax enable

    set ignorecase
    set nocompatible
    set scrolloff=2
    set shiftwidth=2
    set smartcase
    set tabstop=8
    set textwidth=74
    set visualbell
  '';
}
