{ config, pkgs, lib, ... }:

with builtins; with lib; let
  apps = import ./appdaemon/apps.nix { lib = lib; };
  config = import ./appdaemon/config.nix;

  configDir = "/var/lib/appdaemon";

  expandApp = appName: appConfig: {
    name = appName;
    value = rec {
      module = strings.toLower (appConfig.class or appName);
      class = appName;
    } // appConfig;
  };
  expandedApps = listToAttrs (lists.zipListsWith expandApp (attrNames apps) (attrValues apps));

  configToYaml = name: conf:
    let
      configJSON = pkgs.writeText "${name}.json" (builtins.toJSON conf);
    in
      pkgs.runCommand
        "${name}.yml"
        { preferLocalBuild = true; }
        ''
          ${pkgs.remarshal}/bin/json2yaml -i ${configJSON} -o $out
        '';


  appdaemonConfig = configToYaml "appdaemon" config;
  appdaemonAppConfig = configToYaml "apps" expandedApps;
  appdaemonApps = "${./appdaemon/apps}";

  appdaemon = pkgs.appdaemon.overridePythonAttrs(oldAttrs: {
      propagatedBuildInputs = builtins.concatLists [
        [ pkgs.python3.pkgs.setuptools ]
        oldAttrs.propagatedBuildInputs
      ];
    });

in {
  systemd.services.appdaemon = {
    description = "AppDaemon";
    serviceConfig = {
      ExecStart = "${appdaemon}/bin/appdaemon -c '${configDir}'";
      User = "appdaemon";
    };
    preStart =
      ''
        rm -f "${configDir}/appdaemon.yaml"
        ln -s ${appdaemonConfig} "${configDir}/appdaemon.yaml"
        mkdir -p "${configDir}/apps"
        ln -sf ${appdaemonApps}/*.py "${configDir}/apps"
        cp ${appdaemonAppConfig} "${configDir}/apps/apps.yaml"
        chmod 644 "${configDir}/apps/apps.yaml"
      '';
  };

  systemd.targets.appdaemon = rec {
    description = "AppDaemon";
    wantedBy = [ "multi-user.target" ];
    wants = [ "appdaemon.service" ];
    after = wants;
  };

  users.users.appdaemon = {
    home = configDir;
    createHome = true;
    isSystemUser = true;
  };
}
