{ lib }:
with builtins; let
  hide_undefined = all: defined:  
    listToAttrs (map (x: {
        name = x;
        value.hidden = "true";
      }) (attrValues all))
   // defined;

  safe_name = with lib.strings; node: 
    concatMapStringsSep "_" toLower (splitString " " node.name);

in rec {
  
  sensors = 
    let
      cyrus_10077 = {loc, post ? ""}:
        let
          nodeid = "vision_zp3111_multisensor_4in1";
        in rec {
          name = "Sensor ${loc}";
          node = "${nodeid}${post}";
          full_node = "zwave.${node}";
          attributes = {
            battery = 1;
          };
          entities = {
            alarm_type = "sensor.${nodeid}_alarm_type${post}";
            alarm_level = "sensor.${nodeid}_alarm_level${post}";
            burglar = "sensor.${nodeid}_burglar${post}";
            clock = "sensor.${nodeid}_clock${post}";
            luminance = "sensor.${nodeid}_luminance${post}";
            relative_humidity = "sensor.${nodeid}_relative_humidity${post}";
            sensor = "binary_sensor.${nodeid}_sensor${post}";
            sourcenodeid = "sensor.${nodeid}_sourcenodeid${post}";
            system = "sensor.${nodeid}_system${post}";
            temperature = "sensor.${nodeid}_temperature${post}";
            battery = "sensor.battery_${node}";
          };
          customize = hide_undefined entities (with entities; {
            "${battery}" = {};
            "${temperature}" = { friendly_name = "${loc} Temperatur"; };
            "${luminance}" = {
              friendly_name = "${loc} Helligkeit";
              icon = "mdi:weather-sunny";
            };
            "${relative_humidity}" = {
              friendly_name = "${loc} relative Feuchte";
              icon = "mdi:water-percent";
            };
          });
        };
    in {
      bad = cyrus_10077 {
        loc = "Bad";
        
      };
      wohnzimmer = cyrus_10077 {
        loc = "Wohnzimmer";
        post = "_2";
      };
  };

  switches = {
    bad_luefter =
      let 
        nodeid = "qubino_zmnhadx_flush_1_relay";
      in rec {
        name = "Bad Lüfter";
        node = "${nodeid}${post}";
        full_node = "zwave.${node}";
        attributes = {};
        entities = {
          switch = "switch.${nodeid}_switch";
          alarm_level = "sensor.${nodeid}_alarm_level";
          alarm_type = "sensor.${nodeid}_alarm_type";
          energy = "sensor.${nodeid}_energy";
          exporting = "sensor.${nodeid}_exporting";
          int2 = "binary_sensor.${nodeid}_int2";
          int3 = "binary_sensor.${nodeid}_int3";
          power = "sensor.${nodeid}_power";
          power_management = "sensor.${nodeid}_power_management";
          sourcenodeid = "sensor.${nodeid}_sourcenodeid";
        };
        customize = hide_undefined entities (with entities; {
          ${switch} = {
            friendly_name = name;
            icon = "mdi:fan";
          }; 
          ${power} = {
            friendly_name = "${name} Verbrauch";
            icon = "mdi:power-plug";
          };
          ${energy} = {
            friendly_name = "${name} Gesamtverbrauch";
            icon = "mdi:gauge";
          };
        });
    };
  };
  
  thermostats =
    let 
      spiritz = {loc, post ? ""}:
        let
          nodeid = "eurotronic_eur_spiritz_wall_radiator_thermostat";
        in rec {
          name = "Heizung ${loc}";
          node = "${nodeid}${post}";
          full_node = "zwave.${node}";
          attributes = {
            battery = 1;
          };
          entities = {
            alarm_type = "sensor.${nodeid}_alarm_type${post}";
            alarm_level = "sensor.${nodeid}_alarm_level${post}";
            temperature = "sensor.${nodeid}_temperature${post}";
            heat = "climate.${nodeid}_heat${post}"; 
            heat_eco = "climate.${nodeid}_heat_eco${post}"; 
            furnace = "climate.${nodeid}_furnace${post}"; 
            power_management = "sensor.${nodeid}_power_management${post}";
            sourcenodeid = "sensor.${nodeid}_sourcenodeid${post}";
            system = "sensor.${nodeid}_system${post}";
            battery = "sensor.battery_${node}";
          };
          customize = hide_undefined entities (with entities; {
            "${battery}" = {};
            ${temperature} = {
              friendly_name = "${name} Temperatur";
            };
            ${heat} = {
              friendly_name = name;
            };
          });
        };
    in {
      wohnzimmer = spiritz {
        loc = "Wohnzimmer";
        post = "_4";
      };
      bad = spiritz {
        loc = "Bad";
        post = "_2";
      };
      schlafzimmer = spiritz {
        loc = "Schlafzimmer";
        post = "_3";
      };
      arbeitszimmer = spiritz {
        loc = "Arbeitszimmer";
        post = "";
      };
  };

  all = concatLists (map attrValues [sensors switches thermostats]);
}
