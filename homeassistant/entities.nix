{ nodes }: {
  bad = {
    luefter = nodes.switches.bad_luefter.entities.switch;
    temperature = "sensor.vision_zp3111_multisensor_4in1_temperature";
    luminance = "sensor.vision_zp3111_multisensor_4in1_luminanace";
    relative_humidity = "sensor.vision_zp3111_multisensor_4in1_relative_humidity";
    heater = nodes.thermostats.bad.entities.heat;
  };
  schlafzimmer = {
    heater = nodes.thermostats.schlafzimmer.entities.heat;
  };


}
