{ config, lib }:
let
  nodes = import ./nodes.nix { lib = lib; };
  entities = import ./entities.nix { nodes = nodes; };
  secrets = import ../secrets;
in with builtins; {
  binary_sensor = [
    {
      platform = "template";
      sensors = {
        bad_motion = {
          friendly_name = "Bad Bewegung";
          value_template =
          ''
            {{ is_state("${nodes.sensors.bad.entities.burglar}", "8") }}
          '';
          device_class = "motion";
        };
      };
    }
    {
      platform = "ping";
      name = "Internet v4";
      host = "176.9.23.218";
      count = 3;
      scan_interval = 30;
    }
    {
      platform = "ping";
      name = "Internet v6";
      host = "2a01:4f8:150:21c3::be1e";
      count = 3;
      scan_interval = 30;
    }
    {
      name = "Arbeitstag";
      platform = "workday";
      country = "DE";
      province = "BY";
    }
    {
      name = "morgen Arbeitstag";
      platform = "workday";
      country = "DE";
      province = "BY";
      days_offset = 1;
    }
  ];

  climate = [
    {
      platform = "zwave";
    }
  ];

  config = { };

  device_tracker = [
    {
      platform = "luci";
      host = secrets.router.host;
      username = secrets.router.username;
      password = secrets.router.password;
    }
  ];

  frontend = { };

  homeassistant = {
    name = "Home";
    unit_system = "metric";
    time_zone = config.time.timeZone;
    latitude = secrets.homeassistant.latitude;
    longitude = secrets.homeassistant.longitude;
    elevation = secrets.homeassistant.elevation;

    customize = with entities; foldl' (x: y: x // y) {
    } (map (x: x.customize) nodes.all);

    auth_providers = [
      {
        type = "trusted_networks";
        trusted_networks = [ "192.168.0.0/24" ];
        trusted_users = {
          "192.168.0.0/24" = [ "d117fc7988184a9a9a58938181736d70" ];
        };
        allow_bypass_login = true;
      }
      {
        type = "homeassistant";
      }
    ];
  };

  http = {
    base_url = "https://${secrets.domain}";
    use_x_forwarded_for = true;
    trusted_proxies = [ "127.0.0.1" "::1" ];
    ip_ban_enabled = true;
    login_attempts_threshold = 10;
  };

  input_datetime = {
    aufstehen = {
      name = "Aufstehzeit";
      has_time = true;
      has_date = false;
      icon = "mdi:alarm";
    };
  };

  logger = {
    default = "info";
    logs = {
      mysensors = "debug";
      "homeassistant.compontents.mysensors" = "debug";
    };
  };

  mqtt = {
    broker = "127.0.0.1";
    port = config.services.mosquitto.port;
    username = "hass";
    password = secrets.mqtt.hass.password;
    discovery = true;
  };

  mysensors = {
    gateways = {
      device = "mqtt";
      topic_in_prefix = "mysensors-in";
      topic_out_prefix = "mysensors-out";
      persistence_file = "/var/lib/hass/mysensors.json";
    };
    version = "2.3";
  };

  neato = secrets.neato;

  owntracks = {
    max_gps_accuracy = 50;
  };

  person = { };

  sensor = [
    {
      platform = "dwd_weather_warnings";
      region_name = "München";
      name = "Wetterwarnungen";
    }
    {
      platform = "template";
      sensors =
        let 
          sensor_name = node: (lib.strings.removePrefix "sensor." node.entities.battery);
          batterySensor = node: {
            "${sensor_name node}" = {
              friendly_name = "${node.name} Batterie";
              unit_of_measurement = "percent";
              device_class = "battery";
              value_template = "{{ state_attr('${node.full_node}', 'battery_level') }}";
            };
          };
        in
          lib.lists.foldr (a: b: b // (batterySensor a)) {} (lib.lists.partition
           (lib.attrsets.hasAttrByPath [ "attributes" "battery" ])
           nodes.all).right;
    }
  ];

  sun = { };

  system_health = { };

  websocket_api = { };

  weather = [ {
    platform = "openweathermap";
    mode = "freedaily";
    api_key = secrets.owm_api_key;
  } ];

  zeroconf = { };

  zwave = {
    usb_path = "/dev/ttyACM0";
    network_key = secrets.zwave.network_key;
  };
}
