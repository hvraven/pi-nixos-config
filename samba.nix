{ ... }: {
  users.users.share = {
    home = "/data";
    uid = 2000;
    group = "share";
  };

  users.groups.share = {
    gid = 2000;
  };

  services.samba = {
    enable = true;
    enableNmbd = true;
    enableWinbindd = false;

    securityType = "user";

    extraConfig = ''
      map to guest = bad user
      guest account = share
    '';

    shares = {
      data = {
        browsable = "yes";
        "guest ok" = "yes";
        "read only" = "no";
        path = "/data";
        "create mask" = "0666";
        "directory mask" = "0777";
        "force user" = "share";
        "force group" = "share";
      };
    };
  };

  networking.firewall = {
    allowedTCPPorts = [ 139 445 ];
    allowedUDPPorts = [ 137 138 ];
  };
}
