{ lib }:
let
  nodes = import ../homeassistant/nodes.nix { lib = lib; };
  entities = import ../homeassistant/entities.nix { nodes = nodes; };

  make_battery_alert = node:
    if lib.attrsets.hasAttrByPath ["entities" "battery"] node then {
      ${node.node} = {
        class = "BatteryAlert";
        battery = node.entities.battery;
        pretty_name = node.name;
      };
    } else {};

  batteryAlerts = lib.lists.foldr (l: r: r // (make_battery_alert l)) {}  nodes.all;
in {
  BadLuefter = {
    fan = entities.bad.luefter;
  };

  BathHeater = {
    class = "TimedHeater";
    heater = entities.bad.heater;
    on_timer = "input_datetime.aufstehen";
    on_duration = "02:00:00";
    heat_temperature = 25.0;
    eco_temperature = 20.0;
    constrain_input_boolean = "binary_sensor.arbeitstag";
  };

  SchlafzimmerHeater = {
    class = "TimedHeater";
    heater = entities.schlafzimmer.heater;
    on_time = "22:00:00";
    on_duration = "08:00:00";
    heat_temperature = 19.0;
    eco_temperature = 16.0;
  };

  GetUpTimer = {
    workday_time = "07:00";
    holiday_time = "09:00";
    timer = "input_datetime.aufstehen";
    tomorrow_workday_sensor = "binary_sensor.morgen_arbeitstag";
  };
} // batteryAlerts
