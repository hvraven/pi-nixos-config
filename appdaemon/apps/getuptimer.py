import appdaemon.plugins.hass.hassapi as hass
import datetime


class GetUpTimer(hass.Hass):
    def initialize(self):
        self.timer = self.args["timer"]
        self.workday_time = self.args["workday_time"]
        self.holiday_time = self.args["holiday_time"]
        self.tomorrow_workday_sensor = self.args["tomorrow_workday_sensor"]

        self.run_daily(self.reset_getup_time_callback, datetime.time(10))

    def reset_getup_time_callback(self, kwargs):
        workday = self.get_state(self.tomorrow_workday_sensor) == "on"
        if workday:
            time = self.workday_time
        else:
            time = self.holiday_time

        self.call_service(
            "input_datetime/set_datetime", entity_id=self.timer, time=time
        )
