import appdaemon.plugins.hass.hassapi as hass


class BatteryAlert(hass.Hass):
    def initialize(self):
        self.battery = self.args["battery"]
        self.pretty_name = self.args["pretty_name"]
        self.alert_level = int(self.args.get("alert_level", 25))
        self.listen_state(self.battery_state_callback, self.battery)
        self.battery_state_callback(None, None, None, self.get_state(self.battery), {})

    def battery_state_callback(self, entity, attribute, old, new, kwargs):
        if int(new) <= self.alert_level:
            self.log(f"battery alert: {new}% for {self.battery}")
            self.call_service(
                "persistent_notification/create",
                message=f"Batterie von {self.pretty_name} bei {new}%",
                title="Batteriewarnung",
                notification_id=self.battery,
            )
        else:
            self.log(f"battery good: {new}% for {self.battery}")
