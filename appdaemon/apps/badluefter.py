import appdaemon.plugins.hass.hassapi as hass


class BadLuefter(hass.Hass):
    def initialize(self):
        self.fan = self.args["fan"]
        self.shut_off_timer = None
        self.listen_state(self.switched_callback, self.fan)
        if self.get_state(self.fan) == "on":
            self.turn_off_in_3_hours()

    def switched_callback(self, entity, attribute, old, new, kwargs):
        if old == "off" and new == "on":
            self.turn_off_in_3_hours()
        elif old == "on" and new == "off":
            if self.shut_off_timer:
                self.cancel_timer(self.shut_off_timer)
                self.shut_off_timer == None

    def turn_off_in_3_hours(self):
        self.log("scheduling shut off in 3 hours")
        if self.shut_off_timer:
            self.cancel_timer(self.shut_off_timer)
        self.shut_off_timer = self.run_in(self.turn_off_callback, 60 * 60 * 3)

    def turn_off_callback(self, kwargs):
        self.log(f"shutting off {self.fan}")
        self.turn_off(self.fan)
