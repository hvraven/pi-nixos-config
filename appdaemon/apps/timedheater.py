import appdaemon.plugins.hass.hassapi as hass
import datetime

def parse_time(time, offset=datetime.timedelta()):
    return datetime.datetime.time(
            datetime.datetime.strptime(time, "%H:%M:%S") + offset)

class TimedHeater(hass.Hass):
    def initialize(self):
        self.heater = self.args["heater"]
        self.on_time = self.args.get("on_time")
        self.heat_temperature = self.args["heat_temperature"]
        self.eco_temperature = self.args["eco_temperature"]
        self.on_duration = self.parse_time(self.args["on_duration"])
        self.pre_start = datetime.timedelta(hours=-1*self.args.get("pre_start", 0))

        if not self.on_time:
            self.listen_state(self.on_time_changed_callback, self.args["on_timer"])
            self.on_time = self._on_time_from_timer
        else:
            self.on_time = parse_time(self.on_time)

        self.listen_state(
            self.heater_changed_callback, self.heater, attribute="temperature"
        )

        self.on_timer_handle = None
        self.on_handle = None

        # fake callback for setting first timer
        self.on_time_changed_callback(None, None, None, str(self.on_time), {})
        self.heater_changed_callback(
            None, None, None, self.get_state(self.heater, attribute="temperature"), {}
        )

    @property
    def _on_time_from_timer(self):
        return self.get_state(self.args["on_timer"])

    def on_time_changed_callback(self, entity, attribute, old, new, kwargs):
        if self.on_timer_handle:
            self.cancel_timer(self.on_timer_handle)

        new_time = datetime.datetime.combine(self.datetime(), self.parse_time(new))
        new_time -= self.pre_start

        self.log(f"setting timer for {self.heater} to {new_time.time()}")
        self.on_timer_handle = self.run_daily(
            self.change_temperature_callback,
            new_time.time(),
            temperature=self.heat_temperature,
        )

    def change_temperature_callback(self, kwargs):
        temperature = kwargs["temperature"]
        self.call_service(
            "climate/set_temperature", entity_id=self.heater, temperature=temperature
        )
        self.log(f"setting {self.heater} to {temperature}C")
        if temperature <= self.eco_temperature and self.on_handle:
            self.log("canceling on timer")
            self.cancel_timer(self.on_handle)

    def heater_changed_callback(self, entity, attribute, old, new, kwargs):
        self.log(f"heater_changed_callback(new={new})")
        if self.on_handle:
            self.cancel_timer(self.on_handle)
        if float(new) > self.eco_temperature:
            t = self.on_duration
            delay = (t.hour * 60 + t.minute) * 60 + t.second
            self.log(f"setting off timer in {delay}s")
            self.on_handle = self.run_in(
                self.change_temperature_callback,
                delay,
                temperature=self.eco_temperature,
            )
