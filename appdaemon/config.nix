let
  secrets = import ../secrets;
in {
  appdaemon = {
    plugins = {
      HASS = {
        type = "hass";
        ha_url = "http://localhost:8123";
        token = secrets.token.appdaemon;
      };
    };
  };
}
