{ config, pkgs, lib, ... }:

let
  secrets = import ./secrets;

  mysensors = pkgs.mysensors.override {
    password = secrets.mqtt.mysensors.password;
    mqtt_address = "127.0.0.1";
    mqtt_port = config.services.mosquitto.port;
  };

  mySensorsConfig = pkgs.writeText "mysensors.conf"
  ''
    verbose=debug
    log_file=0
    syslog=1
    log_pipe=0
    eeprom_file=/var/lib/mysensors/mysensors.eeprom
    eeprom_size=1024
    soft_hmac_key=${secrets.mysensors.hmac}
    aes_key=${secrets.mysensors.aes}
    soft_serial_key=${secrets.mysensors.serial}
  '';

  mySensorsPrepare = pkgs.writeText "mysensors-spidev.sh"
  ''
    [[ -d /sys/kernel/config/device-tree/overlays/spi0 ]] || \
      mkdir -p /sys/kernel/config/device-tree/overlays/spi0 && \
      cat /boot/overlays/spi0-hw-cs.dtbo > \
        /sys/kernel/config/device-tree/overlays/spi0/dtbo
  '';
in {

  systemd.services.mysensors = {
    description = "MySensors Gateway";
    after = [ "mosquitto.target" ];
    requisite = [ "mysensors-prepare.service" ];
    serviceConfig = {
      ExecStart = "${mysensors}/bin/mysgw -q --config ${mySensorsConfig}";
      RemoveIPC = true;
    };
  };

  systemd.services.mysensors-prepare = {
    description = "Prepare spidev for mysensors";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.bash}/bin/bash ${mySensorsPrepare}";
      RemainAfterExit = true;
      User = "root";
      Type = "oneshot";
    };
  };

  systemd.targets.mysensors = rec {
    description = "MySensors Gateway";
    wantedBy = [ "multi-user.target" ];
    wants = [ "mysensors.service" ];
    after = wants;
  };
}
