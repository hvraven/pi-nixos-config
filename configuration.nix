{ config, pkgs, lib, options, ... }:

let
  secrets = import ./secrets;
in {
  nixpkgs.overlays = [ (import ./overlays) ];

  nix.nixPath =
    options.nix.nixPath.default ++
    [ "nixpkgs-overlays=/etc/nixos/overlays" ]
  ;

  nixpkgs.config = {
    packageOverrides = pkgs: {
      unstable = import <nixos-unstable> {
        config = config.nixpkgs.config;
      };
    };
  };

  imports = [
    ./appdaemon.nix
    ./homeassistant.nix
    ./samba.nix
  ];

  boot.loader.grub.enable = false;
  boot.loader.generic-extlinux-compatible.enable = false;
  boot.loader.raspberryPi = {
    enable = true;
    version = 3;
    uboot.enable = true;
  };

  boot.kernelPackages = pkgs.linuxPackages_rpi;

  boot.kernelParams = ["cma=32M"];

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-label/NIXOS_BOOT";
      fsType = "vfat";
    };
    "/" = {
      device = "/dev/disk/by-label/NIXOS_DISK";
      fsType = "ext4";
    };
  };

  swapDevices = [ {
    device = "/swap";
    size = 1024;
  } ];

  hardware.enableRedistributableFirmware = true;
  networking.wireless.enable = true;
  networking.wireless.networks = secrets.wifiNetworks;

  networking.hostName = "home-assistant";
  networking.firewall = {
    allowPing = true;
    allowedTCPPorts = [80 443];
  };

  time.timeZone = "Europe/Berlin";

  environment.systemPackages = with pkgs; with python3Packages; [
    (toPythonApplication black)
    git
    gnupg
    htop
    kbfs
    keybase
    screen
    tmux
    mosquitto
    tcpdump
    (import ./vim.nix)
  ];

  environment.variables.EDITOR = "vim";

  services.openssh.enable = true;  

  users.mutableUsers = false;

  users.users.hendrik = {
    isNormalUser = true;
    home = "/home/hendrik";
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIJ3NuM7av4XFlZpgbVU3Q6gzeATCBPB0FDqs2U3zOsM hendrik@psyche"
    ];
    hashedPassword = secrets.hendrikPw;
  };

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIJ3NuM7av4XFlZpgbVU3Q6gzeATCBPB0FDqs2U3zOsM hendrik@psyche" ];

  documentation.info.enable = false;
  documentation.doc.enable = false;

  services.chrony.enable = true;
  networking.timeServers = [
    "ptbtime1.ptb.de"
    "ptbtime2.ptb.de"
    "ptbtime3.ptb.de"
  ];

  services.cron = {
    enable = true;
    systemCronJobs = [
      "*/5 * * * * ${pkgs.curl}/bin/curl 'https://www.duckdns.org/update?domain=${secrets.duckdns.domain}&token=${secrets.duckdns.token}&ip=' > /dev/null"
    ];
  };

  services.nginx = {
    enable = true;
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;

    virtualHosts."${secrets.domain}" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:8123";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_buffering off;
        '';
      };
    };

    virtualHosts."grafana.${secrets.domain}" = {
      enableACME = true;
      forceSSL = true;
      root = "/var/empty";
      locations."/" = {
        proxyPass = "http://localhost:3000";
      };
    };
  };

  security.acme.certs = {
    "${secrets.domain}".email = secrets.admin_email;
    "grafana.${secrets.domain}".email = secrets.admin_email;
  };

  services.journald.console = "/dev/tty1";
}
