{ lib
, buildPythonPackage
, fetchPypi
, pyserial
}:

buildPythonPackage rec {
  pname = "pyserial-asyncio";
  version = "0.4";

  src = fetchPypi {
    inherit pname version;
    sha256 = "c40677a8874d8c24d4423a97498746de776f6dbcd0efbb8fa43dcf011a589aee";
  };

  propagatedBuildInputs = [
    pyserial
  ]; 

  meta = with lib; {
    description = "asyncio extension package for pyserial";
    homepage = "https://github.com/pyserial/pyserial-asyncio";
    license = licenses.bsd3;
  };
}
