{ lib
, pythonPackages
, buildPythonPackage
, fetchPypi
, crcmod
, get-mac
, intelhex
, pyserial
, pyserial-asyncio
, voluptuous
}:

buildPythonPackage rec {
  pname = "pymysensors";
  version = "0.18.0";

  src = fetchPypi {
    inherit pname version;
    sha256 = "0cd4e47daf48a1f63722c3f9dfbff1b54c26ddd8bef1048dfaaeadeedf5c6ded";
  };

  propagatedBuildInputs = [
    crcmod
    get-mac
    intelhex
    pyserial
    pyserial-asyncio
    voluptuous
  ]; 

  meta = {
    description = "Python API for talking to a MySensors gateway";
    homepage = https://github.com/theolind/pymysensors;
    license = lib.licenses.mit;
  };
}
