{ lib
, buildPythonPackage
, fetchPypi
}:

buildPythonPackage rec {
  pname = "get-mac";
  version = "0.8.1";

  src = fetchPypi {
    inherit pname version;
    sha256 = "15bf93a1752403be68a8a6a09ba5c55f1ab9e43d0150706275484d210d187b03";
  };

  doCheck = false;

  meta = with lib; {
    description = "Platform-independent pure-Python module to get a MAC address";
    homepage = "https://github.com/GhostofGoes/getmac";
    license = licenses.mit;
  };
}
