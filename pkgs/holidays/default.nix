{ lib
, buildPythonPackage
, fetchPypi
, dateutil
, six
}:

buildPythonPackage rec {
  pname = "holidays";
  version = "0.9.11";

  src = fetchPypi {
    inherit pname version;
    sha256 = "915fdb92b596cfb66067010759b4384a9c6bc82931311d5bf07fe04920cc11bc";
  };

  propagatedBuildInputs = [
    dateutil
    six
  ];

  meta = with lib; {
    description = "Generate and work with holidays in Python";
    homepage = "https://pypi.python.org/pypi/holidays";
    license = licenses.mit;
  };
}
