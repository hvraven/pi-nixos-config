{ config, pkgs, lib, ... }:

let
  secrets = import ./secrets;

  mqtt_domain = "mqtt.${secrets.domain}";

  tasmota_mqtt_config = name: {
    acl = [
      "topic read sonoffs/cmnd/#"
      "topic read tasmota/${name}/cmnd/#"
      "topic write tasmota/${name}/stat/#"
      "topic write tasmota/${name}/tele/#"
      "topic read cmnd/sonoffs/#"
      "topic read cmnd/tasmota/${name}/#"
      "topic write tele/tasmota/${name}/#"
      "topic write stat/tasmota/${name}/#"
      "topic write homeassistant/#"
    ];
    password = secrets.mqtt.tasmota.${name}.password;
  };

in {
  # dialout group to access zwave usb stick
  users.users.hass.extraGroups = ["dialout"];

  services.home-assistant = {
    enable = true;
    openFirewall = true;

    package = pkgs.home-assistant.override {
      extraPackages = ps: with ps; [
         pymysensors
        holidays
      ];

      packageOverrides = self: super: {
        get-mac = super.callPackage ./pkgs/get-mac { };
        holidays = super.callPackage ./pkgs/holidays { };
        pymysensors = super.callPackage ./pkgs/pymysensors { };
        pyserial-asyncio = super.callPackage ./pkgs/pyserial-asyncio { };

        pynacl = super.pynacl.overridePythonAttrs (oldattrs: {
          # test suite is to much for the poor pi
          doCheck = false;
        });
      };
    };

    config = import ./homeassistant/config.nix {
      config = config;
      lib = lib;
    };
  };

  services.mosquitto = {
    enable = true;
    allowAnonymous = false;
    checkPasswords = true;
    host = "0.0.0.0";
    port = 1883;
    ssl =
      let
        certdir = "/var/lib/acme/${mqtt_domain}";
      in {
        enable = false;
        port = 8883;
	host = "0.0.0.0";
        cafile = "${certdir}/chain.pem";
        certfile = "${certdir}/cert.pem";
	keyfile = "${certdir}/key.pem";
      };
    users = {
      hass = {
        acl = [ "topic readwrite #" ];
        password = secrets.mqtt.hass.password;
      };
      mysensors = {
        acl = [
          "topic read mysensors-in/#"
          "topic write mysensors-out/#"
        ];
        password = secrets.mqtt.mysensors.password;
      };
      octoPrint = {
        acl = [
          "topic write octoPrint/#"
        ];
        password = secrets.mqtt.octoPrint.password;
      };
      owntracks = {
        acl = [
          "topic write owntracks/#"
        ];
        password = secrets.mqtt.owntracks.password;
      };
      tasmota_obi01 = tasmota_mqtt_config "obi01";
      tasmota_obi02 = tasmota_mqtt_config "obi02";
    };
  };

  networking.firewall.allowedTCPPorts = [ config.services.mosquitto.port ];

  services.nginx.virtualHosts."${mqtt_domain}" = {
    enableACME = true;
    forceSSL = true;
    root = "/var/empty";
  };

  security.acme.certs."${mqtt_domain}" = {
    email = secrets.admin_email;
    group = "mosquitto";
    allowKeysForGroup = true;
    plugins = [ "fullchain.pem" "full.pem" "chain.pem" "cert.pem" "key.pem" "account_key.json" ];
    postRun = "systemctl reload mosquitto.service";
  };

  services.prometheus = {
    enable = false;
    scrapeConfigs = [ {
      job_name = "hass";
      scrape_interval = "60s";
      metrics_path = "/api/prometheus";
      bearer_token = secrets.token.prometheus;
      static_configs = [ {
        targets = [
          "localhost:8123"
        ];
      } ];
    } ];
  };

  services.grafana = {
    enable = false;
    domain = "grafana.${secrets.domain}";
    analytics.reporting.enable = false;
    security = secrets.grafana;
    provision = {
      enable = true;
      datasources = [ {
        name = "prometheus";
        type = "prometheus";
        url = "http://localhost:9090";
        isDefault = true;
      } ];
    };
  };

  systemd.targets.home-assistant.after = [
    "mosquitto.service"
  ];

}
