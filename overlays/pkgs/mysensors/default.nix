{ stdenv
, fetchFromGitHub
, bash
, password ? ""
, mqtt_address ? "127.0.0.1"
, mqtt_port ? 1883
}:

stdenv.mkDerivation rec {
  name = "mysensors-${version}";
  # development 2019aug02
  version = "baf48678221789070eda6e67671e569d4b965701";

  src = fetchFromGitHub {
    owner = "mysensors";
    repo = "MySensors";
    rev = version;
    sha256 = "1gzl89dca11j8nsmgdp566xy5ny0ap9x77w53s1fsm9qn03sf2h0";
  };

  patches = [
    ./fix-64bit-compile.patch
  ];

  nativeBuildInputs = [ bash ];

  configureScript = "${bash}/bin/bash ./configure";
  configureFlags = [
    "--soc=BCM2837"
    "--cpu-flags=-Wall"
    "--spi-driver=SPIDEV"
    "--spi-spidev-device=/dev/spidev0.0"
    "--my-transport=rfm69"
    "--my-rfm69-frequency=868"
    "--my-rfm69-encryption-enabled"
    "--my-signing=software"
    "--my-signing-request-signatures"
    "--my-gateway=mqtt"
    "--my-controller-ip-address=${mqtt_address}"
    "--my-port=${toString mqtt_port}"
    "--my-mqtt-publish-topic-prefix=mysensors-out"
    "--my-mqtt-subscribe-topic-prefix=mysensors-in"
    "--my-mqtt-client-id=mysensors"
    "--my-mqtt-user=mysensors"
    "--my-mqtt-password=${password}"
  ];

  installPhase = 
  ''
    mkdir -p $out/bin/
    cp bin/mysgw $out/bin/
  '';
}
