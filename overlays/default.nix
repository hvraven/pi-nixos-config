self: super:

{
  mysensors = super.callPackage ./pkgs/mysensors {
    stdenv = self.stdenv;
    fetchFromGitHub = self.fetchFromGitHub;
    bash = self.bash;
  };
}
